﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L01.Logic
{
    public class PaginatedList<T> : List<T>
    {
        public static int _pegeIndex { get; set; }
        public static int _totalPages { get; set; }
        public static int _totalItem { get; set; }

        public PaginatedList(List<T> soure, int count, int index, int pageSize)
        {
            _totalItem = count;
            _pegeIndex = index;
            _totalPages = (int)Math.Ceiling(count / (double)pageSize);
            AddRange(soure);
        }
        public static async Task<PaginatedList<T>> Paginated(IQueryable<T> soure, int pageIndex, int pageSize)
        {
            var count = await soure.CountAsync();
            var item = await soure
                       .Skip((pageIndex - 1) * pageSize)
                       .Take(pageSize)
                       .ToListAsync();
            return new PaginatedList<T>(item, count, pageIndex, pageSize);
        }
        public static bool HasPreviousPage
        {
            get { return (_pegeIndex > 1); }
        }
        public static bool HasNextPage
        {
            get { return (_pegeIndex < _totalPages); }
        }
    }
}
