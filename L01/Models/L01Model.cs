﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L01.Models
{
    public class BaseModel
    {
        public int Id { get; set; }
        public DateTime Created_date { get; set; } = DateTime.Now;
        public DateTime Updated_date { get; set; } = DateTime.Now;
        public bool Active { get; set; } = true;
    }

    public class Customer : BaseModel
    {
        public int CustomerTypeid { get; set; } 
        public string Name { get; set; }
        public string Code { get; set; }
        public string Sex { get; set; }
        public DateTime dob { get; set; }
        public string IdCard { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Attachment { get; set; }
        public string images { get; set; }
    }
    public class CustomerType : BaseModel
    {
        public string Name { get; set; }
        public int ParentId { get; set; }
    }

    public class LoanType : BaseModel
    {
        public string Name { get; set; }
        public decimal Rate { get; set; }
        public string RateType { get; set; }
        public int Calculation { get; set; }
        public int Period { get; set; }
        public string PeriodType { get; set; }
        public int Payback { get; set; }
        public string PaybackType { get; set; }
        public int RepayAfter { get; set; }
    }

    public class Loan : BaseModel
    {
        public int CustomerId { get; set; }
        public int ContractId { get; set; }
        public int LoanTypeId { get; set; }
        public decimal Amount { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal Balance { get; set; }
        public int CurrencyId { get; set; }
        public int Period { get; set; }
        public string PeriodType { get; set; }
        public decimal Rate { get; set; }
        public string RateType { get; set; }
        public DateTime LoanDate { get; set; }
        public DateTime ScheduleStartDate { get; set; }
        public string Status { get; set; }
    }
}
