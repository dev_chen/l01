﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using L01.Models;
using Microsoft.AspNetCore.Mvc;
using L01.Logic;
namespace L01.Controllers
{
    public class CustomerController : Controller
    {
        [HttpGet]
        public async Task<IActionResult> Index(int? page)
        {
            int pageSize = 10;
            return View(await PaginatedList<Customer>.Paginated(CustomerLogic.Instance.All(),page ?? 1, pageSize));
        }
        [HttpGet]
        public async Task<IActionResult> Add()
        {
            var customerType = await CustomerTypeLogic.Instance.Bind();
            ViewBag.customer = customerType;
            return View();
        }
        [HttpPost]
        public async Task<ActionResult<Customer>> Add(Customer customer)
        {
            await CustomerLogic.Instance.Add(customer);
            return RedirectToAction(nameof(Index));
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var q = await CustomerLogic.Instance.Find(id);
            var customerType = await CustomerTypeLogic.Instance.Bind();
            ViewBag.customer = customerType;
            return View(q);
        }
        [HttpPost]
        public async Task<ActionResult<Customer>> Edit(Customer customer)
        {
            await CustomerLogic.Instance.Edit(customer);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> Remove(int id)
        {
            return View(await CustomerLogic.Instance.Find(id));
        }
        public async Task<ActionResult> Delete(int id)
        {
            await CustomerLogic.Instance.Remove(id);
            return RedirectToAction(nameof(Index));
        }
    }
}