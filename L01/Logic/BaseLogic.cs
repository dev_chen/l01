﻿using L01.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L01.Logic
{
    public class BaseLogic<T> where T : BaseModel
    {
        public static L01Context _db = new L01Context();
        public virtual IQueryable<T> All()
        {
            var entity = _db.Set<T>().Where(x => x.Active).AsNoTracking();
            return entity;
        }
        public virtual async Task<List<T>> Search(string search)
        {
            return await _db.Set<T>().Where(x => x.Active).AsNoTracking().ToListAsync();
        }
        public virtual async Task<T> Find(int id)
        {
            var entity = await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            if (entity?.Active ?? false)
            {
                return entity;
            }
            else
            {
                return null;
            }
        }
        public virtual async Task<T> Add(T entity)
        {
            _db.Entry(entity).State = EntityState.Detached;
            _db.Entry(entity).State = EntityState.Added;
            await _db.SaveChangesAsync();
            return entity;
        }
        public virtual async Task<T> Edit(T entity)
        {
            _db.Entry(entity).State = EntityState.Detached;
            var cur = await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == entity.Id);
            if (entity.Active)
            {
               _db.Entry(cur).CurrentValues.SetValues(entity);
            }
            else
            {
                cur.Active = false;
            }
            await _db.SaveChangesAsync();
            return entity;
        }
        public virtual async Task<T> Remove(T entity)
        {
            entity.Active = false;
            return await  Edit(entity);
        }
        public virtual async Task<T> Remove(int id)
        {
            var entity = await _db.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            entity.Active = false;
            await Edit(entity);
            return entity;
        }
    }
}
