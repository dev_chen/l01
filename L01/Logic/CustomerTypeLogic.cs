﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using L01.Models;
using Microsoft.EntityFrameworkCore;

namespace L01.Logic
{
    public class CustomerTypeLogic : BaseLogic<CustomerType>
    {
        public static CustomerTypeLogic Instance = new CustomerTypeLogic();
        public async Task<List<CustomerType>> Bind()
        {
            var customerType = await _db.CustomerTypes.Where(x => x.Active).AsNoTracking().ToListAsync();
            return customerType;
        }
    }
}
