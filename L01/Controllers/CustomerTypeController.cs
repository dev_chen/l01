﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using L01.Models;
using L01.Logic;
using Microsoft.EntityFrameworkCore;
namespace L01.Controllers
{
    public class CustomerTypeController : Controller
    {
        public async Task<IActionResult> Index(int? page)
        {
            int pageSize = 25;
            return View(await PaginatedList<CustomerType>.Paginated(CustomerTypeLogic.Instance.All(), page ?? 1 , pageSize));
        }
        [HttpGet]
        public async Task<IActionResult> Add()
        {
            var customerType = await CustomerTypeLogic.Instance.Bind();
            ViewBag.item = customerType;
            return View();
        }
        [HttpPost]
        public async Task<ActionResult<CustomerType>> Add(CustomerType customerType)
        {
            await CustomerTypeLogic.Instance.Add(customerType);
            return RedirectToAction(nameof(Index));
        }
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            // bing data to Select tags
            var customerType = await CustomerTypeLogic.Instance.Bind();
            ViewBag.item = customerType;
            var q = await CustomerTypeLogic.Instance.Find(id);
            return View(q); 
        }
        [HttpPost]
        public async Task<ActionResult<CustomerType>> Edit(CustomerType customerType)
        {
            await CustomerTypeLogic.Instance.Edit(customerType);
            return RedirectToAction(nameof(Index));
        }
        [HttpGet]
        public async Task<ActionResult<CustomerType>> Remove(int id)
        {
            var q = await CustomerTypeLogic.Instance.Find(id);
            return View(q);
        }
        public async Task<ActionResult<CustomerType>> Delete(int id)
        {
            await CustomerTypeLogic.Instance.Remove(id);
            return RedirectToAction(nameof(Index));
        }
    }
}