﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace L01.Migrations
{
    public partial class v1000 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created_date = table.Column<DateTime>(nullable: false),
                    Updated_date = table.Column<DateTime>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CustomerTypeid = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Sex = table.Column<string>(nullable: true),
                    dob = table.Column<DateTime>(nullable: false),
                    IdCard = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Attachment = table.Column<string>(nullable: true),
                    images = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CustomerTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created_date = table.Column<DateTime>(nullable: false),
                    Updated_date = table.Column<DateTime>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ParentId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Loans",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created_date = table.Column<DateTime>(nullable: false),
                    Updated_date = table.Column<DateTime>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    CustomerId = table.Column<int>(nullable: false),
                    ContractId = table.Column<int>(nullable: false),
                    LoanTypeId = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    PaidAmount = table.Column<decimal>(nullable: false),
                    Balance = table.Column<decimal>(nullable: false),
                    CurrencyId = table.Column<int>(nullable: false),
                    Period = table.Column<int>(nullable: false),
                    PeriodType = table.Column<string>(nullable: true),
                    Rate = table.Column<decimal>(nullable: false),
                    RateType = table.Column<string>(nullable: true),
                    LoanDate = table.Column<DateTime>(nullable: false),
                    ScheduleStartDate = table.Column<DateTime>(nullable: false),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Loans", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LoanTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created_date = table.Column<DateTime>(nullable: false),
                    Updated_date = table.Column<DateTime>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Rate = table.Column<decimal>(nullable: false),
                    RateType = table.Column<string>(nullable: true),
                    Calculation = table.Column<int>(nullable: false),
                    Period = table.Column<int>(nullable: false),
                    PeriodType = table.Column<string>(nullable: true),
                    Payback = table.Column<int>(nullable: false),
                    PaybackType = table.Column<string>(nullable: true),
                    RepayAfter = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LoanTypes", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "CustomerTypes");

            migrationBuilder.DropTable(
                name: "Loans");

            migrationBuilder.DropTable(
                name: "LoanTypes");
        }
    }
}
