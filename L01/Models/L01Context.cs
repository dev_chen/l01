﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace L01.Models
{
    public class L01Context : DbContext
    {
        public L01Context() { }
        public L01Context(DbContextOptions<L01Context> options) : base(options){}
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=KH-CHEN\\MSSQLSERVER2016;Database=l01;Trusted_Connection=True;");
            base.OnConfiguring(optionsBuilder);
        }
        public DbSet<CustomerType> CustomerTypes { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<LoanType> LoanTypes { get; set; }
        public DbSet<Loan> Loans { get; set; }
    }
}
